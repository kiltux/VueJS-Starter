import path from 'path';
import webpack from 'webpack';
import webpackMerge from "webpack-merge";

import generalConfig from "./webpack.config";

const config: webpack.Configuration = {
    devtool: "inline-source-map",
    mode: "development",
    devServer: {
        contentBase: path.join(__dirname, "assets"),
        hotOnly: true,
        historyApiFallback: true,
        noInfo: true,
        port: 9000,
        overlay: {
            errors: true,
            warnings: true
        },
        publicPath: generalConfig.output.publicPath
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
    ]
};

export default webpackMerge(generalConfig, config);