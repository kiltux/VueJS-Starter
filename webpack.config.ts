import path from 'path';
import webpack from 'webpack';
import VueLoaderPlugin from 'vue-loader';

const config: webpack.Configuration = {
    entry: [
        "./src/main.ts"
    ],    
    output: {
        filename: "app.js",
        path: path.resolve(__dirname, 'dist'),        
        publicPath: "/dist/"
    },
    module: {
        rules: [
            { 
                test: /\.ts$/,
                loader: "ts-loader", 
                exclude: "/node_modules/",
                options: { appendTsSuffixTo: [/\.vue$/] }
            },
            {
                test: /\.vue$/,
                loader: "vue-loader", 
                exclude: "/node_modules/",
                options: {
                    loaders: {
                      // Since sass-loader (weirdly) has SCSS as its default parse mode, we map
                      // the "scss" and "sass" values for the lang attribute to the right configs here.
                      // other preprocessors should work out of the box, no loader config like this necessary.
                      'scss': 'vue-style-loader!css-loader!sass-loader',
                      'sass': 'vue-style-loader!css-loader!sass-loader?indentedSyntax',
                    }
                    // other vue-loader options go here
                  }
            },
            {
                test: /\.css$/,
                loaders: ['vue-style-loader', 'css-loader']
            }
        ]
    },
    plugins: [
        new VueLoaderPlugin.VueLoaderPlugin(),
    ],
    resolve: {
      extensions: ['.ts', '.js', '.vue', '.json'],
      alias: {
        'vue$': 'vue/dist/vue.esm.js'
      }
    }
};

export default config;